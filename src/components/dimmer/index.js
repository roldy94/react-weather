import { DimmerWrapper } from './styles';

const Dimmer = ({ ...props }) => <DimmerWrapper {...props} />;

export default Dimmer;
