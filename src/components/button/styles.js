import styled from 'styled-components';
import { Button } from 'semantic-ui-react';

export const ButtonWrapper = styled(Button)`
  width: 35px;
  height: 35px;
`;
