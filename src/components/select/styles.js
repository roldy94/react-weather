import styled from 'styled-components';
import { Select } from 'semantic-ui-react';

export const SelectWrapper = styled(Select)`
  width: 50%;
`;
