import { SelectWrapper } from './styles';

const Select = ({ ...props }) => <SelectWrapper {...props} />;

export default Select;
