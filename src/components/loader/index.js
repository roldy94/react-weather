import { LoaderWrapper } from './styles';

const Loader = ({ ...props }) => <LoaderWrapper {...props} />;

export default Loader;
