import { ListWrapper } from './styles';

const List = ({ ...props }) => <ListWrapper {...props} />;

export default List;
