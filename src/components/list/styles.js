import styled from 'styled-components';
import { List } from '@material-ui/core';

export const ListWrapper = styled(List)`
  display: flex;
`;
