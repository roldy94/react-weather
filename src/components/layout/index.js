import { LayoutWrapper, Container, Flex, SelectFlex } from './styles';
import { Dimmer, Loader, Select } from '../../components';
import Weather from '../../feature/weather';
import { Forecast } from '../../feature/forecast';

const Layout = ({ ...props }) => {
  const { isFetching, state, onChange } = props;

  return (
    <LayoutWrapper {...props}>
      {!isFetching ? (
        <Container>
          <SelectFlex>
            <Select
              placeholder='Select your country'
              options={state.options}
              onChange={(e, data) => onChange(data.value)}
              value={state.selected}
            />
          </SelectFlex>
          <Weather weatherData={state.weather} />
          <Forecast />
        </Container>
      ) : (
        <Flex>
          <Dimmer active>
            <Loader>Loading..</Loader>
          </Dimmer>
        </Flex>
      )}
    </LayoutWrapper>
  );
};

export default Layout;
