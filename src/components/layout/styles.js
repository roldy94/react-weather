import styled from 'styled-components';

export const LayoutWrapper = styled.div`
  background-color: #eeeeee;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: black;
`;

export const Container = styled.div`
  width: 60%;
`;

export const Flex = styled.div``;
export const SelectFlex = styled.div`
  text-align: center;
`;
