import React, { useRef } from 'react';
import {
  faCloud,
  faBolt,
  faCloudRain,
  faCloudShowersHeavy,
  faSnowflake,
  faSun,
  faSmog,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const renderIcon = ({ name, ref }) => {
  switch (name) {
    case 'Thunderstorm':
      return <FontAwesomeIcon icon={faBolt} />;

    case 'Drizzle':
      return <FontAwesomeIcon icon={faCloudRain} />;

    case 'Rain':
      return <FontAwesomeIcon icon={faCloudShowersHeavy} />;

    case 'Snow':
      return <FontAwesomeIcon icon={faSnowflake} />;

    case 'Clear':
      return <FontAwesomeIcon icon={faSun} />;

    case 'Clouds':
      return <FontAwesomeIcon icon={faCloud} />;

    default:
      return <FontAwesomeIcon icon={faSmog} />;
  }
};

const Icon = ({ name }) => {
  const iconRef = useRef();
  return renderIcon({ name, ref: iconRef });
};

export default Icon;
