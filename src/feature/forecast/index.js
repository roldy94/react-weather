import React from 'react';
import { useSelector } from 'react-redux';
import moment from 'moment';
import { Icon, List } from '../../components';
import { WeatherIcon, ForecastContainer, Layout, Flex, Text } from './styles';

export function Forecast() {
  const state = useSelector((state) => state.forecastReducer);
  // const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(Actions.forecastRequest({ lat, long }));
  // }, []);

  const _renderList = () => {
    return state.forecast.map((item, index) => {
      return (
        <ForecastContainer key={index}>
          <Flex>
            <Text>{moment(item.dt_txt).format('dddd')}</Text>
            <WeatherIcon>{<Icon name={item.description} />}</WeatherIcon>
            <Text>{item.temperature} &deg;C</Text>
          </Flex>
        </ForecastContainer>
      );
    });
  };

  return (
    <Layout>
      <List aria-label='forecast data'>
        {!state.isFetching ? <_renderList /> : null}
      </List>
    </Layout>
  );
}
