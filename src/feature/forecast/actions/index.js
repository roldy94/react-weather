import makeActionCreator from '../../../redux/utils';

export const FORECAST_REQUEST = 'FORECAST_REQUEST';
export const FORECAST_SUCCESS = 'FORECAST_SUCCESS';

const forecastRequest = makeActionCreator(FORECAST_REQUEST, 'payload');
const forecastSuccess = makeActionCreator(FORECAST_SUCCESS, 'payload');

export const ActionsTypes = {
  FORECAST_REQUEST,
  FORECAST_SUCCESS,
};

export const Actions = {
  forecastRequest,
  forecastSuccess,
};

export const ActionsForecast = {
  forecastRequest,
};
