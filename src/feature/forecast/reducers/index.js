import { ActionsTypes } from '../actions';

const initialState = {
  forecast: [],
  isFetching: false,
};

export default function forecastReducer(state = initialState, action = {}) {
  switch (action.type) {
    case ActionsTypes.FORECAST_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case ActionsTypes.FORECAST_SUCCESS:
      return {
        ...state,
        isFetching: false,
        forecast: action.payload,
      };

    default:
      return state;
  }
}
