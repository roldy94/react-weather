import { call, put, takeEvery } from 'redux-saga/effects';
import { ActionsTypes,Actions } from './actions';
import { getForecast } from '../../api/forecastSerivce';

function* requestWorker(action) {
  try {
    const data = {
      lat: action.payload.lat,
      long: action.payload.long,
    };
    const list = yield call(getForecast, data);
    yield put(Actions.forecastSuccess(list));
  } catch (e) {
    console.error(e);
  }
}

function* forecastSaga() {
  yield takeEvery(ActionsTypes.FORECAST_REQUEST, requestWorker);
}

export default forecastSaga;
