import styled from 'styled-components';

export const ForecastContainer = styled.div`
  border-radius: 20px;
  background-color: #01579b;
  color: whitesmoke;
  padding: 10px;
  margin: 10px;
  font-family: 'Recursive', sans-serif;
  font-size: 19px;
  width: 100%;
`;

export const WeatherIcon = styled.div`
  color: whitesmoke;
  font-size: 25;
  margin-top: 4;
`;

export const Layout = styled.div``;

export const Flex = styled.div`
  display: initial;
  justify-content: space-between;
`;

export const Text = styled.p``;
