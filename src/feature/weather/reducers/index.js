import { ActionsTypes } from '../actions';

const initialState = {
  weather: [],
  isFetching: true,
  options: [],
  select: '',
};

export default function weatherReducer(state = initialState, action = {}) {
  switch (action.type) {
    case ActionsTypes.WEATHER_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case ActionsTypes.WEATHER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        weather: action.payload,
        options: [
          {
            key: Math.random(),
            value: { lat: 40.73061, long: 139.839478 },
            text: 'Tokyo',
          },
          {
            key: Math.random(),
            value: { lat: -22.908333, long: -43.196388 },
            text: 'Rio de Janeiro',
          },
          {
            key: Math.random(),
            value: { lat: 51.509865, long: -0.118092 },
            text: 'London',
          },
          {
            key: Math.random(),
            value: { lat: 48.864716, long: 2.349014 },
            text: 'Paris',
          },
          {
            key: Math.random(),
            value: { lat: 40.73061, long: -73.935242 },
            text: 'New York',
          },
        ],
        //select: state.options[0],
      };

    default:
      return state;
  }
}
