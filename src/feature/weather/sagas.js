import { call, put, takeEvery } from 'redux-saga/effects';
import { ActionsTypes, Actions } from './actions';
import { getWeather } from '../../api/weatherService';

function* requestWorker(action) {
  try {
    const data = {
      lat: action.payload.lat,
      long: action.payload.long,
    };
    const list = yield call(getWeather, data);
    yield put(Actions.weatherSuccess(list));
  } catch (e) {
    console.error(e);
  }
}

function* weatherSaga() {
  yield takeEvery(ActionsTypes.WEATHER_REQUEST, requestWorker);
}

export default weatherSaga;
