import makeActionCreator from '../../../redux/utils';

export const WEATHER_REQUEST = 'WEATHER_REQUEST';
export const WEATHER_SUCCESS = 'WEATHER_SUCCESS';

const weatherRequest = makeActionCreator(WEATHER_REQUEST, 'payload');
const weatherSuccess = makeActionCreator(WEATHER_SUCCESS, 'payload');

export const ActionsTypes = {
  WEATHER_REQUEST,
  WEATHER_SUCCESS,
};

export const Actions = {
  weatherRequest,
  weatherSuccess,
};
