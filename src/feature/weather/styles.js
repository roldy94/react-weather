import styled from 'styled-components';

export const WeatherIcon = styled.div`
  color: whitesmoke;
  font-size: 25;
  margin-top: 4;
`;

export const Layout = styled.div`
  margin: 10px;
  border-radius: 20px;
  background-color: #ca5050;
`;

export const Top = styled.div`
  height: 60px;
  background-color: #424242;
  color: whitesmoke;
  padding: 10px;
  border-radius: 20px 20px 0 0;
  font-family: 'Recursive', sans-serif;
  display: flex;
  justify-content: space-between;
`;

export const Flex = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Header = styled.p`
  background-color: #424242;
  color: whitesmoke;
  margin: 10px 0px 0px 10px;
  font-size: 25px;
  border-radius: 20px 20px 0 0;
  font-family: 'Recursive', sans-serif;
`;

export const Day = styled.p`
  padding: 15px;
  color: whitesmoke;
  font-family: 'Recursive', sans-serif;
  font-size: 18px;
  font-weight: 600;
`;

export const Description = styled.p`
  padding: 15px;
  color: whitesmoke;
  font-family: 'Recursive', sans-serif;
  font-size: 24px;
  font-weight: 600;
`;

export const SunriseSunset = styled.p`
  padding: 15px;
  color: whitesmoke;
  font-family: 'Recursive', sans-serif;
  font-size: 16px;
`;

export const Temp = styled.p`
  padding: 15px;
  color: whitesmoke;
  font-family: 'Recursive', sans-serif;
  font-size: 18px;
`;
