import React from 'react';
import moment from 'moment';
import { Button, Icon } from '../../components';
import {
  WeatherIcon,
  Layout,
  Top,
  Flex,
  Header,
  Day,
  Description,
  SunriseSunset,
  Temp,
} from './styles';

export default function Weather({ weatherData }) {
  
  const refresh = () => {
    window.location.reload();
  };

  return (
    <Layout>
      <Top>
        <Header>{weatherData.name}</Header>
        <Button
          className='button'
          inverted
          color='blue'
          circular
          icon='refresh'
          onClick={refresh}
        />
      </Top>
      <Flex>
        <Day>
          {moment().format('dddd')}, <span>{moment().format('LL')}</span>
        </Day>
        <Flex>
          <WeatherIcon style={{ fontSize: 30, marginTop: 15 }}>
            {<Icon name={weatherData.weather[0].main} />}
          </WeatherIcon>
          <Description>{weatherData.weather[0].main}</Description>
        </Flex>
      </Flex>

      <Flex>
        <Temp>Temprature: {weatherData.main.temp} &deg;C</Temp>
        <Temp>Humidity: {weatherData.main.humidity} %</Temp>
      </Flex>

      <Flex>
        <SunriseSunset>
          Sunrise:{' '}
          {new Date(weatherData.sys.sunrise * 1000).toLocaleTimeString('en-IN')}
        </SunriseSunset>
        <SunriseSunset>
          Sunset:{' '}
          {new Date(weatherData.sys.sunset * 1000).toLocaleTimeString('en-IN')}
        </SunriseSunset>
      </Flex>
    </Layout>
  );
}
