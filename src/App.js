import './App.css';
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Actions } from './feature/weather/actions';
import { ActionsForecast } from './feature/forecast/actions';
import { Layout } from './components';

export default function App() {
  const [lat, setLat] = useState([]);
  const [long, setLong] = useState([]);

  const state = useSelector((state) => state.weatherReducer);
  const dispatch = useDispatch();

  const _handleChange = (data) => {
    let lat = data.lat;
    let long = data.long;
    dispatch(Actions.weatherRequest({ lat, long }));
    dispatch(ActionsForecast.forecastRequest({ lat, long }));
  };
  useEffect(() => {
    navigator.geolocation.getCurrentPosition(function (position) {
      setLat(position.coords.latitude);
      setLong(position.coords.longitude);
    });

    if (lat.length !== 0 && long.length !== 0) {
      dispatch(Actions.weatherRequest({ lat, long }));
      dispatch(ActionsForecast.forecastRequest({ lat, long }));
    }
  }, [lat, long]);

  return (
    <Layout
      isFetching={state.isFetching}
      state={state}
      lat={lat}
      long={long}
      onChange={_handleChange}
    />
  );
}
