export function getForecast(data) {
  return fetch(
    `${process.env.REACT_APP_API_URL}/forecast/?lat=${data.lat}&lon=${data.long}&units=metric&APPID=${process.env.REACT_APP_API_KEY}`
  )
    .then((res) => handleResponse(res))
    .then((result) => {
      if (Object.entries(result).length) {
        return result.list
          .filter((forecast) => forecast.dt_txt.match(/09:00:00/))
          .map(mapDataToWeatherInterface);
      }
    });
}

function handleResponse(response) {
  if (response.ok) {
    return response.json();
  } else {
    throw new Error('Please Enable your Location in your browser!');
  }
}

function mapDataToWeatherInterface(data) {
  const mapped = {
    date: data.dt * 1000, // convert from seconds to milliseconds
    description: data.weather[0].main,
    temperature: Math.round(data.main.temp),
  };

  // Add extra properties for the five day forecast: dt_txt, icon, min, max
  if (data.dt_txt) {
    mapped.dt_txt = data.dt_txt;
  }

  return mapped;
}
