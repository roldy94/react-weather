export function getWeather(params) {
  return fetch(
    `${process.env.REACT_APP_API_URL}/weather/?lat=${params.lat}&lon=${params.long}&units=metric&APPID=${process.env.REACT_APP_API_KEY}`
  )
    .then((res) => handleResponse(res))
    .then((result) => {
      return result;
    });
}

function handleResponse(response) {
  if (response.ok) {
    return response.json();
  } else {
    throw new Error('Please Enable your Location in your browser!');
  }
}
