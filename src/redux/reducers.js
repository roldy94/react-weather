import forecastReducer from '../feature/forecast/reducers';
import weatherReducer from '../feature/weather/reducers';

export default {
  forecastReducer,
  weatherReducer,
};
