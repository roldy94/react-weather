import { all } from 'redux-saga/effects';
import forecastSaga from '../feature/forecast/sagas';
import weatherSaga from '../feature/weather/sagas';

export default function* rootSaga() {
  yield all([weatherSaga(), forecastSaga()]);
}
