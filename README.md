# React Weather

This project is a web application made with react

## Getting Started

Follow the steps below

### Prerequisites

The latest stable version of node

```
https://nodejs.org/es/
```

### Installing

Clone the project first

```
git clone https://gitlab.com/roldy94/react-weather
```

After installing the node modolues

```
npm install
```

## Running 

To start the application

```
npm start
```

## Versioning

V 1.0

## Authors

* **Nahuel Roldan** - *Initial work* - [Software Developer](https://gitlab.com/roldy94)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

   * "@fortawesome/fontawesome-svg-core": "^1.2.34",
   * "@fortawesome/free-solid-svg-icons": "^5.15.2",
   * "@fortawesome/react-fontawesome": "^0.1.14",
   * "@material-ui/core": "^4.11.3",
   * "@testing-library/jest-dom": "^5.11.4",
   * "@testing-library/react": "^11.1.0",
   * "@testing-library/user-event": "^12.1.10",
   * "axios": "^0.21.1",
   * "gh-pages": "^3.1.0",
   * "moment": "^2.29.1",
   * "react": "^17.0.1",
   * "react-dom": "^17.0.1",
   * "react-scripts": "4.0.3",
   * "semantic-ui-css": "^2.4.1",
   * "semantic-ui-react": "^2.0.3",
   * "styled-components": "^5.2.1",
   * "web-vitals": "^1.0.1"
